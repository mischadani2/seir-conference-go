import json

# provides functions for working with JSON data

import requests

# allows us to send HTTP requests and handle responses


from .models import ConferenceVO


def get_conferences():
    url = "http://monolith:8000/api/conferences/"
    # This line assigns the URL of the API endpoint for conferences to the variable "url"

    response = requests.get(url)
    # This line sends an HTTP GET response to the URL using requests.get() function and assigns the response to variable "response"

    content = json.loads(response.content)
    # This line loads the content of the HTTP response as a JSON string, using json.oads in JSON string and t
    for conference in content["conferences"]:
        ConferenceVO.objects.update_or_create(
            import_href=conference["href"],
            defaults={"name": conference["name"]},
        )
