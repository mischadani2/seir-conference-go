from datetime import datetime
from json import JSONEncoder
from django.db.models import QuerySet


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        # if o is an instance of a QuerySet, just turn it into a normal list rather than the fancy list that it already is.
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class DateEncoder(JSONEncoder):
    def default(self, o):
        # if o is an instance of datetime
        if isinstance(o, datetime):
            return o.isoformat()
        # otherwise
        else:
            return super().default(o)


class ModelEncoder(QuerySetEncoder, DateEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        #   if the object to decode is the same class as what's in the
        #   model property, then
        if isinstance(o, self.model):
            #     * create an empty dictionary that will hold the property names
            #       as keys and the property values as values
            d = {}
            # if o has the attribute get_api_url
            if hasattr(o, "get_api_url"):
                #    then add its return value to the dictionary
                #    with the key "href"
                d["href"] = o.get_api_url()
            #     * for each name in the properties list
            for property in self.properties:
                #         * get the value of that property from the model instance
                #           given just the property name
                value = getattr(o, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    print("******************", value)
                    value = encoder.default(value)
                #         * put it into the dictionary with that property name as
                #           the key
                d[property] = value
            d.update(self.get_extra_data(o))
            #     * return the dictionary
            return d
        #   otherwise,
        else:
            return super().default(o)  # From the documentation

    def get_extra_data(self, o):
        # Now, wherever you want to add extra data, you only need to add that method to the specific model encoder that you're building. For example, the api_show_location function view is supposed to return the two-letter state abbreviation in its return value.
        return {}
